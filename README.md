# __APPLICATION__NAME__

This project was generated with [angular-cli](https://github.com/angular/angular-cli).

## Development server
Run `npm run serve` for start dev server. Navigate to `http://localhost:4200/`.
The app will automatically reload if you change any of the source files.

## Build

Run `npm run start` to build the project for dev environment, app will be rebuilt if you change any of the source files.
Run `npm run build` to build the project for prod environment.

The build artifacts will be stored in the `dist/` directory.

## Format Typescript files

Run `npm run format` to execute the clang formatter. This will format all Typescript (*.ts) files according to .clang-format.

## Lint Typescript files

Run `npm run lint` to execute the Typescript linter. This will lint all Typescript (*.ts) files according to tslint.json.

## Running unit tests

Run `npm run test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `npm run e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `npm run serve`.

## Further reading

* [Angular.io](https://angular.io/)
* [Angular-CLI README](https://github.com/angular/angular-cli/blob/master/README.md)
* [rxjs](http://reactivex.io/rxjs/manual/overview.html)
* [ngrx](https://gist.github.com/btroncone/a6e4347326749f938510)
