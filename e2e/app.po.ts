import { browser, element, by } from 'protractor';

export class AngularSeedPage {
  public navigateTo(): any {
    return browser.get('');
  }

  public getParagraphText(): any {
    return element(by.css('app-root ul li')).getText();
  }
}
