declare interface Environment {
    APP_BASE_HREF: string;
    CSN: string;
    LOCALE: string;
    IS_DEVELOPMENT: boolean;
    AUTHORIZATION: string;
    API: string;
}

declare let ENV: Environment;
