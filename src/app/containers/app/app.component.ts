import {Component, OnInit, ViewContainerRef, ViewEncapsulation} from '@angular/core';
import {Observable} from 'rxjs/Observable';

import {AppService, StateService} from '../../services';


@Component({
    selector: 'app-root',
    styleUrls: ['app.component.scss'],
    templateUrl: 'app.component.html',
    encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
    public loading$: Observable<boolean>;

    public constructor(
        private appService: AppService, private stateService: StateService,
        private viewContainerRef: ViewContainerRef) {}

    public ngOnInit(): void {
        this.appService.setRootViewContainerRef(this.viewContainerRef);
        this.appService.init();
        this.loading$ = this.stateService.statesLoaded$.map(isLoaded => !isLoaded);
    }
}
