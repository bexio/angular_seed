import {AppService} from '../../services';

export class AppServiceMock {
    public init(): void {}
}

export const AppServiceMockProvider = {
    provide: AppService,
    useClass: AppServiceMock
};
