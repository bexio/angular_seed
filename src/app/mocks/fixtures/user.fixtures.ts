import {plainToClass} from 'class-transformer';

import {User} from '../../models';

export const client = plainToClass(User, {'name': 'Steve'});
