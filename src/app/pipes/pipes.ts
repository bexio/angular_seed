import {SafeHtmlPipe} from './safe-html.pipe';
import {ToNumberPipe} from './to-number.pipe';

export {ToNumberPipe} from './to-number.pipe';

export const appPipes = [SafeHtmlPipe, ToNumberPipe];
