import * as userActions from '../actions/user.action';
import {User} from '../models';

export interface State {
    client: User;
    loaded: boolean;
}

const initialState: State = {
    client: Object.assign(new User(), {name: ''}),
    loaded: true
};

export const reducer = (state = initialState, action: userActions.Actions): State => {
    switch (action.type) {
        case userActions.LOAD_SUCCESS:
            return {client: action.payload, loaded: true};
        case userActions.LOAD_FAIL:
            return Object.assign({}, state, {loaded: true});
        default:
            return state;
    }
};

export const getClient = (state: State) => state.client;

export const getLoaded = (state: State) => state.loaded;
