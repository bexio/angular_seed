import {compose} from '@ngrx/core/compose';
import {routerReducer, RouterState} from '@ngrx/router-store';
import {ActionReducer, combineReducers} from '@ngrx/store';
import {storeFreeze} from 'ngrx-store-freeze';
import {createSelector} from 'reselect';

import {environment} from '../../environments/environment';

import * as fromUser from './user.reducer';

export interface State {
    router: RouterState;
    user: fromUser.State;
}

export const reducers = {
    router: routerReducer,
    user: fromUser.reducer
};

/**
 * User
 */
export const getUserState = (state: State) => state.user;

export const getUserClient = createSelector(getUserState, fromUser.getClient);

export const getUserLoaded = createSelector(getUserState, fromUser.getLoaded);

/**
 * App reducer
 */
const developmentReducer: ActionReducer<State> = compose(storeFreeze, combineReducers)(reducers);
const productionReducer: ActionReducer<State> = combineReducers(reducers);

export function reducer(state: any, action: any): any {
    if (environment.production) {
        return productionReducer(state, action);
    } else {
        return developmentReducer(state, action);
    }
}
