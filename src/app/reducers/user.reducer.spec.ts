import * as userActions from '../actions/user.action';
import * as userReducerFixtures from '../mocks/fixtures/user.fixtures';
import {User} from '../models';

import {reducer} from './user.reducer';

const initialState = {
    client: null,
    loaded: false
};

describe('User reducer', () => {
    it('should have initial state', () => {
        const initalClient = Object.assign(new User(), {name: ''});
        const state = undefined;
        const action = {type: null};
        expect(reducer(state, action)).toEqual({client: initalClient, loaded: true});
    });

    it('should return existing state on load', () => {
        const state = initialState;
        const action = new userActions.LoadAction();
        expect(reducer(state, action)).toBe(state);
    });

    it('should return loaded true on load fail', () => {
        const state = initialState;
        const action = new userActions.LoadFailAction();
        expect(reducer(state, action)).toEqual({client: null, loaded: true});
    });

    it('should return new state on load success', () => {
        const state = initialState;
        const action = new userActions.LoadSuccessAction(userReducerFixtures.client);
        expect(reducer(state, action)).toEqual({client: userReducerFixtures.client, loaded: true});
    });

});
