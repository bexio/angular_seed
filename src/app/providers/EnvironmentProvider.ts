import {APP_BASE_HREF} from '@angular/common';
import {LOCALE_ID} from '@angular/core';

import * as TOKENS from './tokens';

const defaultApi = '';
const defaultAuthorization = '';

let APP_GLOBAL_ENV = <Environment>{};

if (typeof ENV === 'object') {
    APP_GLOBAL_ENV = ENV;
}

export function getEnvironmentProviders(): any[] {
    return [
        {provide: APP_BASE_HREF, useValue: APP_GLOBAL_ENV.APP_BASE_HREF},
        {provide: LOCALE_ID, useValue: APP_GLOBAL_ENV.LOCALE},
        {provide: TOKENS.csnToken, useValue: APP_GLOBAL_ENV.CSN},
        {provide: TOKENS.isDevelopmentToken, useValue: APP_GLOBAL_ENV.IS_DEVELOPMENT},
        {provide: TOKENS.apiToken, useValue: APP_GLOBAL_ENV.API || defaultApi}, {
            provide: TOKENS.authorizationToken,
            useValue: APP_GLOBAL_ENV.AUTHORIZATION || defaultAuthorization
        }
    ];
}
