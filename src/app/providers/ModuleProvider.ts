import {ToastOptions} from 'ng2-toastr';
import {CustomToastOptions} from '../modules/CustomToastOptions';

export function getModuleProviders(): any[] {
    return [{provide: ToastOptions, useClass: CustomToastOptions}];
}
