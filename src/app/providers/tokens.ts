import {InjectionToken} from '@angular/core';

export const csnToken = new InjectionToken('CSN');
export const isDevelopmentToken = new InjectionToken('IS_DEVELOPMENT');
export const authorizationToken = new InjectionToken('AUTHORIZATION');
export const apiToken = new InjectionToken('API');
