/*tslint:disable:typedef*/
import {Action} from '@ngrx/store';

import {User} from '../models';

export const LOAD = '[User] Load';
export const LOAD_SUCCESS = '[User] Load Success';
export const LOAD_FAIL = '[User] Load Fail';


export class LoadAction implements Action { public readonly type = LOAD; }

export class LoadSuccessAction implements Action {
    public readonly type = LOAD_SUCCESS;

    public constructor(public payload: User) {}
}

export class LoadFailAction implements Action { public readonly type = LOAD_FAIL; }

export type Actions = LoadAction | LoadSuccessAction | LoadFailAction;
