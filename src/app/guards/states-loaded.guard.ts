import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/take';

import {Injectable} from '@angular/core';
import {CanActivate} from '@angular/router';
import {Observable} from 'rxjs/Observable';

import {StateService} from '../services';

@Injectable()
export class StatesLoadedGuard implements CanActivate {
    public constructor(private stateService: StateService) {}

    private waitForStatesLoaded(): Observable<boolean> {
        return this.stateService.statesLoaded$.filter(isLoaded => isLoaded).take(1);
    }

    public canActivate(): Observable<boolean> {
        return this.waitForStatesLoaded();
    }
}
