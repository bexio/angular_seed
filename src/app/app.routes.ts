import {RouterModule, Routes} from '@angular/router';
import {ListComponent} from './components';
import {StatesLoadedGuard} from './guards';

export const routes: Routes = [
    {path: '', canActivate: [StatesLoadedGuard], component: ListComponent},
    {path: '**', redirectTo: ''}
];

export const appRoutes = RouterModule.forRoot(routes);
