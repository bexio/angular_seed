import 'hammerjs';

import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {MaterialModule} from '@angular/material';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BexioModule} from '@bexio/common';
import {RouterStoreModule} from '@ngrx/router-store';
import {StoreModule} from '@ngrx/store';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';

import {appRoutes} from './app.routes';
import {appComponents, appEntryComponents} from './components';
import {AppComponent, appContainers} from './containers';
import {appDirectives} from './directives';
import {appEffects} from './effects';
import {appGuards} from './guards';
import {appModules} from './modules';
import {appPipes} from './pipes';
import {getEnvironmentProviders, getModuleProviders} from './providers';
import {reducer} from './reducers';
import {appServices} from './services';


@NgModule({
    declarations: [...appContainers, ...appComponents, ...appDirectives, ...appPipes],
    imports: [
        BrowserModule, BrowserAnimationsModule, BexioModule.forRoot(), FormsModule, ReactiveFormsModule, HttpModule,
        MaterialModule, StoreModule.provideStore(reducer),
        StoreDevtoolsModule.instrumentOnlyWithExtension(), ...appEffects, appRoutes,
        RouterStoreModule.connectRouter(), ...appModules
    ],
    entryComponents: [...appEntryComponents],
    providers:
        [...appServices, ...getEnvironmentProviders(), ...appGuards, ...getModuleProviders()],
    bootstrap: [AppComponent]
})
export class AppModule {
}
