import {EffectsModule} from '@ngrx/effects';
import {UserEffects} from './user.effects';

export const appEffects = [EffectsModule.run(UserEffects)];
