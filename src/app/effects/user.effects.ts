import 'rxjs/add/operator/switchMap';

import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import {Action} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';

import * as userActions from '../actions/user.action';
import {ApiService} from '../services';

@Injectable()
export class UserEffects {
    @Effect()
    public fetch$: Observable<Action> =
        this.actions$.ofType(userActions.LOAD)
            .switchMap(
                () => this.apiService.getUser()
                          .map(user => new userActions.LoadSuccessAction(user))
                          .catch(() => Observable.of(new userActions.LoadFailAction())));

    public constructor(private actions$: Actions, private apiService: ApiService) {}
}
