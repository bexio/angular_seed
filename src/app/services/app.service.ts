import {Inject, Injectable, LOCALE_ID, ViewContainerRef} from '@angular/core';
import {DOCUMENT} from '@angular/platform-browser';
import {TranslateService} from 'ng2-translate';
import {MessageService} from './message.service';
import {StateService} from './state.service';

const defaultLocale = 'de';

@Injectable()
export class AppService {
    private rootViewContainerRef: ViewContainerRef;

    public constructor(
        private translateService: TranslateService, private messageService: MessageService,
        private stateService: StateService, @Inject(DOCUMENT) private document: any,
        @Inject(LOCALE_ID) private locale: string) {}

    public setRootViewContainerRef(viewContainerRef: ViewContainerRef): void {
        this.rootViewContainerRef = viewContainerRef;
    }

    public init(): void {
        this.initTranslateService();
        this.initMessageService();
    }

    private initTranslateService(): void {
        const shortLocale = this.locale.split('-')[0];
        this.translateService.setDefaultLang(defaultLocale);
        this.translateService.use(shortLocale);
    }

    private initMessageService(): void {
        this.messageService.init(this.rootViewContainerRef);
        this.messageService.action$.subscribe(action => this.stateService.dispatch(action));
    }
}
