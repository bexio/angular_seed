import {Injectable, ViewContainerRef} from '@angular/core';
import {ToastsManager} from 'ng2-toastr/ng2-toastr';
import {Observable} from 'rxjs/Observable';

const defaultOptions = {
    toastLife: 4000,
    showCloseButton: true
};

const errorOptions = {
    dismiss: 'click'
};

const mergeOptions = (...options) => Object.assign({}, defaultOptions, ...options);

@Injectable()
export class MessageService {
    public constructor(private toastsManager: ToastsManager) {}

    public init(viewContainerRef: ViewContainerRef): void {
        this.toastsManager.setRootViewContainerRef(viewContainerRef);
    }

    public get action$(): Observable<any> {
        return this.toastsManager.onClickToast()
            .filter(toast => toast.data && toast.data['action'])
            .map(toast => toast.data['action']);
    }

    public info(message: string, title?: string, options?: any): void {
        this.toastsManager.info(message, title, mergeOptions(options));
    }

    public success(message: string, title?: string, options?: any): void {
        this.toastsManager.success(message, title, mergeOptions(options));
    }

    public warning(message: string, title?: string, options?: any): void {
        this.toastsManager.warning(message, title, mergeOptions(options));
    }

    public error(message: string, title?: string, options?: any): void {
        this.toastsManager.error(message, title, mergeOptions(errorOptions, options));
    }
}
