import {ApiService} from './api.service';
import {AppService} from './app.service';
import {MessageService} from './message.service';
import {StateService} from './state.service';

export const appServices = [ApiService, AppService, MessageService, StateService];
