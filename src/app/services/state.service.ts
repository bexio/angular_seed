import 'rxjs/add/observable/combineLatest';
import 'rxjs/add/operator/skip';

import {Injectable} from '@angular/core';
import {Action, Store} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';

import {User} from '../models';
import * as fromRoot from '../reducers';
import {identityTrue} from '../util/filters';

@Injectable()
export class StateService {
    public statesLoaded$: Observable<boolean> = Observable.combineLatest(
        this.store.select(fromRoot.getUserLoaded),
        (...isLoadedStates: boolean[]) => isLoadedStates.every(identityTrue));

    public user$: Observable<User> = this.store.select(fromRoot.getUserClient);

    public constructor(private store: Store<fromRoot.State>) {}

    public dispatch(action: Action): void {
        this.store.dispatch(action);
    }
}
