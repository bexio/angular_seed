import 'rxjs/add/operator/publishLast';

import {Inject, Injectable} from '@angular/core';
import {HttpService, UtilitiesService} from '@bexio/common';
import {classToPlain, plainToClass} from 'class-transformer';
import {Observable} from 'rxjs/Observable';

import {User} from '../models';
import {apiToken, csnToken, isDevelopmentToken} from '../providers';

@Injectable()
export class ApiService {
    private api: string;

    public constructor(
        private httpService: HttpService, private utilitiesService: UtilitiesService,
        @Inject(isDevelopmentToken) private isDevelopment: boolean,
        @Inject(apiToken) private baseApi: string, @Inject(csnToken) private csn: string) {
        const apiIndex = isDevelopment ? 'api_dev.php' : 'api.php';
        this.api = `${this.baseApi}/${apiIndex}/${this.csn}`;
    }

    /**
     * Get user.
     *
     * @returns {Observable<User>}
     */
    public getUser(): Observable<User> {
        return this.httpService.get<User>(`${this.api}/user`)
            .map(user => this.utilitiesService.snakeToCamel(user))
            .map(user => plainToClass(User, user));
    }
}
