export const identityTrue = item => !!item;

export const uniqueId = (item, index, arr) =>
    arr.findIndex(current => current.id === item.id) === index;

export const by = (prop: string) => (...props: any[]) => item => props.indexOf(item[prop]) > -1;

export const byId = by('id');

export const or = (a, b) => item => a(item) || b(item);

export const and = (a, b) => item => a(item) && b(item);
