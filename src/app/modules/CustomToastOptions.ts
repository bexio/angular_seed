import {ToastOptions} from 'ng2-toastr';

const maxShown = 100;
const newestOnTop = false;

export class CustomToastOptions extends ToastOptions {
    public maxShown: number = this.maxShown;
    public newestOnTop: boolean = this.newestOnTop;
}
