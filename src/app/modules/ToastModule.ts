import {ToastModule} from 'ng2-toastr/ng2-toastr';

export const toastModule = ToastModule.forRoot();
