import {toastModule} from './ToastModule';
import {translateModule} from './TranslateModule';

export const appModules = [toastModule, translateModule];
