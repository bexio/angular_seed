import {Http} from '@angular/http';
import {TranslateLoader, TranslateStaticLoader} from 'ng2-translate';
import {TranslateModule} from 'ng2-translate';

export function translateLoaderFactory(http: Http): TranslateStaticLoader {
    const date = +new Date();
    return new TranslateStaticLoader(http, 'assets/i18n', `.json?${date}`);
}

export const translateProvider = {
    provide: TranslateLoader,
    useFactory: translateLoaderFactory,
    deps: [Http]
};


export const translateModule = TranslateModule.forRoot(translateProvider);
